import React from 'react';
import { connect } from 'react-redux';
import PasswordChecker from '../components/GuillaumePasswordChecker.js';
import { onGuillaumeChange } from '../actions/guillaumeActions.js';

class PasswordCheckerPage extends React.Component {
  constructor(props) {
    super(props);
    this.onPasswordCheckerChange = this.onPasswordCheckerChange.bind(this);
  }

  onPasswordCheckerChange(value) {
    const { dispatch } = this.props;
    dispatch(onGuillaumeChange(value));
  }

  render() {
    const { passwordCheck: { value } } = this.props;
    const styles={
      stroke: {
        textDecoration: 'line-through'
      }
    };

    return (
      <div>
        This is our showcase page, i'd like to run a nice and pretty password checker field.

        <PasswordChecker onChange={this.onPasswordCheckerChange} value={value} />

        Things to do:
        <ul>
          <li style={styles.stroke}>Add a textfield to the page, <b>as a new react component PasswordChecker</b>, it has no props yet</li>
          <li style={styles.stroke}>Make it pretty using some minimal styles</li>
          <li style={styles.stroke}>Add a value props, and make the textfield start with this value as default</li>
          <li style={styles.stroke}>Bind a onChange function on this field, print the new value in the console, this should prevent the field to update</li>
          <li style={styles.stroke}>The onchange function should now store the value in the component state, and use the initial props as default.</li>
          <li style={styles.stroke}>Now that we have a working onchange function, we'd like to be able to pass a <b>mandatory</b> onChange property as a PasswordChecker prop from our container page. The function is named <b>onPasswordCheckerChange</b> and also print the value to the console.</li>
          <li style={styles.stroke}>Now start a reducer that may content the PassworCchecker value, the reducer is just defining its initialState. The default state value is "Marge Simpson"</li>
          <li style={styles.stroke}>Bind the ShowCasePage as a smart component to the redux store, to recover textfield value in a mapStateToProps function, pass this value to PasswordChecker</li>
          <li style={styles.stroke}>Add an action to reducer to handle a ON_CHANGE action that stores the new value</li>
        </ul>
      </div>
    );
  }
}

PasswordCheckerPage.propTypes = {
  passwordCheck: React.PropTypes.object,
  dispatch: React.PropTypes.func.isRequired
};

export default connect(state => {
  return {
    passwordCheck: state.guillaumeReducer
  };
})(PasswordCheckerPage);
