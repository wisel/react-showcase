import React from 'react';

class PasswordCheckerPage extends React.Component {
  render() {
    return (
      <div>
        This is our showcase page, i'd like to run a nice and pretty password checker field.

        Things to do:
        <ul>
          <li>Add a textfield to the page, <b>as a new react component PasswordChecker</b>, it has no props yet</li>
          <li>Make it pretty using some minimal styles</li>
          <li>Add a value props, and make the textfield start with this value as default</li>
          <li>Bind a onChange function on this field, print the new value in the console, this should prevent the field to update</li>
          <li>The onchange function should now store the value in the component state, and use the initial props as default.</li>
          <li>Now that we have a working onchange function, we'd like to be able to pass a <b>mandatory</b> onChange property as a PasswordChecker prop from our container page. The function is named <b>onPasswordCheckerChange</b> and also print the value to the console.</li>
          <li>Now start a reducer that may content the PassworCchecker value, the reducer is just defining its initialState. The default state value is "Marge Simpson"</li>
          <li>Bind the ShowCasePage as a smart component to the redux store, to recover textfield value in a mapStateToProps function, pass this value to PasswordChecker</li>
          <li>Add an action to reducer to handle a ON_CHANGE action that stores the new value</li>
          <li>Go freestyle, and do stuff if you have enough time to do so.</li>
        </ul>
      </div>
    );
  }
}

export default PasswordCheckerPage;
