import React from 'react';

class GuillaumePasswordChecker extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.state = {
      value: props.value
    };
  }

  handleOnChange(e) {
    console.log(e.target.value);
    this.setState({
      value: e.target.value
    });

    this.props.onChange(e.target.value);
  }

  render () {
    const { value } = this.state;

    const styles = {
      container: {
        border: 'solid 1px grey',
        backgroundColor: 'lightgrey',
        height: '100px',
        display: 'flex',
        flexFlow: 'column nowrap'
      },
      input: {
        padding: '10px'
      }
    };

    return (
      <div style={styles.container}>
        Hey this is Guillaume password checker component:
        <input onChange={this.handleOnChange} type="text" value={value} style={styles.input} />
      </div>
    );
  }
}

GuillaumePasswordChecker.propTypes = {
  value: React.PropTypes.string,
  onChange: React.PropTypes.func.isRequired
};

GuillaumePasswordChecker.defaultProps = {
  value: 'Lisa Simpson'
};

export default GuillaumePasswordChecker;
