import { combineReducers } from 'redux';
import fuelSavingsAppState from './fuelSavings';
import guillaumeReducer from './guillaumeReducer';

const rootReducer = combineReducers({
  fuelSavingsAppState,
  guillaumeReducer
});

export default rootReducer;
