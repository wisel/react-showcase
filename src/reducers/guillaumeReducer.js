import { GUILLAUME_ON_CHANGE } from '../constants/actionTypes';
import objectAssign from 'object-assign';

const initialState = {
  value: 'Marge Simpson'
};

export default function guillaumeReducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    case GUILLAUME_ON_CHANGE:
      return objectAssign({}, state, {value: action.value});

    default:
      return state;
  }
}
