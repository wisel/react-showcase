import * as types from '../constants/actionTypes';

export function onGuillaumeChange(value) {
  return {type: types.GUILLAUME_ON_CHANGE, value};
}
