# Lighthouse React showcase

Based on react-slingshot (https://github.com/coryhouse/react-slingshot/) 

This project is a showcase of React-Redux capabilities and intends to be used as a pair-coding boilerplate for newcomers.
